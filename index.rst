.. MeDLP documentation master file, created by
   sphinx-quickstart on Mon Dec 28 14:20:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MeDLP!
=================================
*Medical Deep Learning Platform*

MeDLP is a `PyTorch <https://pytorch.org/>`_-based open-source platform for solving medical problems using deep-learning techniques.

MeDLP is developed based on `PyTorch <https://pytorch.org/>`_, `Ignite <https://pytorch.org/ignite/>`_ and `MONAI <https://monai.io/>`_.



.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
